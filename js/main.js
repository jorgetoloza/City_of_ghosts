
$(document).ready(function(){
	$('#menuToggle').on('click', function(){
		
		if(!$('body').hasClass('menu-show')){
			$('body').addClass('menu-show');
			$('#menuBg, #menu .bg').css('display', 'block');
			setTimeout(function(){
				$('#menuBg').css('opacity', 0.7);
				$('#menu .bg .init-animation').each(function(){
					this.beginElement();
				});
				var delay = $(window).width() <= 440 ? 510 : 0;
				setTimeout(function(){
					$('#menu > :not(.bg)').css({
						opacity: '1',
						transform: 'translateX(0)'
					});
				}, delay);
			}, 100);
		}else{
			hideMenu();			
		}
	});
	$('#menuBg').on('click', hideMenu);
	function hideMenu(){
		$('#menuBg').css('opacity', '0');
		$('#menu > :not(.bg)').css({
			opacity: '0',
			transform: 'translateX(-10px)'
		});
		var delay = $(window).width() <= 440 ? 510 : 0;
		setTimeout(function(){
			$('#menu .bg .inverse-animation').each(function(){
				this.beginElement();
			});
			setTimeout(function(){
				$('#menuBg, #menu .bg').css('display', 'none');
				$('body').removeClass('menu-show');
			}, delay);
		}, delay);
	}
	setTimeout(function(){
		$('body, html').scrollLeft(0);
	}, 500);
	$('#showLogin').on('click', function(){
		$('.modal-login').css('display', 'flex');
	    setTimeout(function () {
	    	$('.modal-login').css('opacity', 1);
	    	$('.modal-login .window').css({
	    		'opacity': 1,
	    		'transform': 'scale(1)'
	    	});
	    }, 100);
	});
	$('.modal-login').on('click', function(e){
		if($(e.target).closest('.window').length == 0){
			$('.modal-login').css('opacity', 0);
	    	$('.modal-login .window').css({
	    		'opacity': 0,
	    		'transform': 'scale(0.9)'
	    	});
		    setTimeout(function () {
				$('.modal-login').css('display', 'none');
		    }, 510);
		}
	});
	$('.modal-login .header .button').on('click', function(){
		if($(this).hasClass('register') && !$('.modal-login .window').hasClass('show-register')){
			$('.modal-login .register-form').css('display', 'block');
		    setTimeout(function () {
		    	$('.modal-login .window').addClass('show-register');
		    }, 100);
		}
		else if($(this).hasClass('login') && $('.modal-login .window').hasClass('show-register')){
			$('.modal-login .window').removeClass('show-register');
			setTimeout(function () {
				$('.modal-login .register-form').css('display', 'none');
			}, 520);
		}

	});

	$(window).on('resize', function(){
		$('body, html').scrollLeft(0);
	});
	
	
	$('body').on('click', '.tabs .header .button', function () {
        var name = $(this).attr('name');
        $(this).addClass('active').siblings().removeClass('active');
        var $body = $(this).closest('.tabs').find('.body .tab[name="' + name + '"]');
        $body.siblings().removeClass('active');
        $body.addClass('active');
    });
	
});


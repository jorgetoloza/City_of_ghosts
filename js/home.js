var map;
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -34.397, lng: 150.644},
		zoom: 8,
		disableDefaultUI: true
	});
	var factory = new google.maps.LatLng(-34.397, 150.644);
	// marker options
	var marker = new google.maps.Marker({
		position: factory,
		map: map,
		title: "El hombre del rio"
	});
	marker.info = {
		title: "El hombre del rio",
		user: {
			img: 'img/user2.jpg',
			name: 'Edinson Castaño'
		},
		place: 'Santa Marta, Colombia',
		date: '20/06/2018',
		text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut laoris nisi ut a',
	};
	console.log(marker);
	// InfoWindow content
  	var content = 
  		'<div id="iw-container">' +
  			'<div class="iw-header">' +
	  			'<div class="user-img" style="background-image: url(' + marker.info.user.img + ');"></div>' +
		        '<div class="iw-info">' +
		          	'<span class="iw-user">' + marker.info.user.name + '</span>' +
		          	'<span class="iw-place">' + marker.info.place + '</span>' +
		          	'<span class="iw-date">' + marker.info.date + '</span>' +
		          	'<span class="iw-title">' + marker.info.title + '</span>' +
		        '</div>' +
		    '</div>' +		        
			'<p>' + marker.info.text + '</p>' +
		    '<div class="iw-buttons">' +			
		    	'<svg viewBox="0 0 24 32">' +
					'<path d="M 15 5 L 15 15 L 5 15 L 5 17 L 15 17 L 15 27 L 17 27 L 17 17 L 27 17 L 27 15 L 17 15 L 17 5 Z "></path>' +
				'</svg>' +
				'<svg viewBox="0 0 24 24">' +
					'<path d="M 18 2 C 16.34375 2 15 3.34375 15 5 C 15 5.195313 15.027344 5.375 15.0625 5.5625 L 7.9375 9.71875 C 7.414063 9.273438 6.742188 9 6 9 C 4.34375 9 3 10.34375 3 12 C 3 13.65625 4.34375 15 6 15 C 6.742188 15 7.414063 14.726563 7.9375 14.28125 L 15.0625 18.4375 C 15.027344 18.625 15 18.804688 15 19 C 15 20.65625 16.34375 22 18 22 C 19.65625 22 21 20.65625 21 19 C 21 17.34375 19.65625 16 18 16 C 17.257813 16 16.585938 16.273438 16.0625 16.71875 L 8.9375 12.5625 C 8.972656 12.375 9 12.195313 9 12 C 9 11.804688 8.972656 11.625 8.9375 11.4375 L 16.0625 7.28125 C 16.585938 7.726563 17.257813 8 18 8 C 19.65625 8 21 6.65625 21 5 C 21 3.34375 19.65625 2 18 2 Z "></path>' +
				'</svg>' +
				'<svg viewBox="0 0 32 32">' +
					'<path d="M 15 5 L 15 15 L 5 15 L 5 17 L 15 17 L 15 27 L 17 27 L 17 17 L 27 17 L 27 15 L 17 15 L 17 5 Z "></path>' +
				'</svg>' +
		    '</div>' +		        
	    '</div>'
    ;

	// A new Info Window is created and set content
	var infowindow = new google.maps.InfoWindow({
		content: content,

		// Assign a maximum value for the width of the infowindow allows
		// greater control over the various content elements
		maxWidth: 350
	});


	// This event expects a click on a marker
	// When this event is fired the Info Window is opened.
	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	});

	// Event that closes the Info Window with a click on the map
	google.maps.event.addListener(map, 'click', function() {
		infowindow.close();
	});

	// *
	// START INFOWINDOW CUSTOMIZE.
	// The google.maps.event.addListener() event expects
	// the creation of the infowindow HTML structure 'domready'
	// and before the opening of the infowindow, defined styles are applied.
	// *
	google.maps.event.addListener(infowindow, 'domready', function() {

	    // Reference to the DIV that wraps the bottom of infowindow
	    var iwOuter = $('.gm-style-iw');

	    /* Since this div is in a position prior to .gm-div style-iw.
	     * We use jQuery and create a iwBackground variable,
	     * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
	    */
	    var iwBackground = iwOuter.prev();

	    // Removes background shadow DIV
	    iwBackground.children(':nth-child(2)').css({'display' : 'none'});

	    // Removes white background DIV
	    iwBackground.children(':nth-child(4)').css({'display' : 'none'});
	    
	    iwBackground.children(':nth-child(3)').find('div > div').css({'background-color' : 'rgb(66, 66, 66)'});


	    // Moves the infowindow 115px to the right.
	    iwOuter.parent().parent().css({left: '115px'});

	    // Moves the shadow of the arrow 76px to the left margin.
	    iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

	    // Moves the arrow 76px to the left margin.
	    iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

	    // Changes the desired tail shadow color.
	    iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

	    // Reference to the div that groups the close button elements.
	    var iwCloseBtn = iwOuter.next();

	    // Apply the desired effect to the close button
	    iwCloseBtn.addClass('iw-close');

	    // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
	    if($('.iw-content').height() < 140){
	      	$('.iw-bottom-gradient').css({display: 'none'});
	    }

	    // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
	    iwCloseBtn.mouseout(function(){
	    	$(this).css({opacity: '1'});
	    });
	});
}
$(document).ready(function(){
	/*$('#socialTabs .header span').on('click', function(){
		$(this).addClass('active').siblings().removeClass('active');
		$('#socialTabs .container.' + $(this).attr('name')).addClass('active').siblings().removeClass('active');
	});*/
	/*setInterval(function(){
		slideRigth();
	}, 6000);*/
	setTimeout(function(){
		$('.slide:first-child').addClass('active');
		$(window).trigger('resize');
	}, 500);
	$(window).on('resize', function(){
		if($(window).width() > 1100){
			var h = $('#histories .articles .preview .content').height() + $('#histories .articles .preview .img').height() + 50;
			$('#histories .articles ul').height(h);
		}
	});
	function slideRigth(){
		if($('.slide.active').next().hasClass('slide')){
			siguiente = $('.slide.active').next();
			$('.slide.active').removeClass('active');
			siguiente.addClass('active');	
		}else{
			siguiente = $('.slide:nth-child(1)');
			$('.slide.active').removeClass('active');
			siguiente.addClass('active');
		}
	}
	$('#chapters').owlCarousel({
	    //loop:true,
	    margin: 40,
	    stagePadding: 40,
	    nav: true,
	    navText: ['', ''],
	    responsive:{
	        0:{
	            items:1
	        },
	        636:{
	            items:2
	        },
	        1024:{
	            items:3
	        },
	        1300:{
	            items:5
	        },
	        1400:{
	            items:5
	        }
	    }
	})
});